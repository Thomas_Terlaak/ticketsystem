import chalk from 'chalk'

class UserRoute {

    server = null;
    
    constructor(server) {
        this.server = server;
    }

    register() {

        this.server.app.get('/schematic', async (req, res) => {

        })

        this.server.app.get('/schematic/weapons/:weaponid', async (req, res) => {

            try
            {
                let response = await this.server.dbConnector.query("SELECT * FROM schematics_weapon WHERE schematic_id = ?", req.params.weaponid);
                res.json({
                    code: 101,
                    message: "schematic recieved",
                    schematic: response.results
                })
            }
            catch(err)
            {

                console.log(chalk.bold(chalk.red(err)))

                res.status(404).json({
                    code: 404,
                    message: "id doesn't exists"
                })
                
            }

        })

        this.server.app.get('/schematic/armor/:armorid', async (req, res) => {

            try
            {
                let response = await this.server.dbConnector.query("SELECT * FROM schematics_armor WHERE schematic_id = ?", req.params.armorid);
                
                if(response.results.length != 0)
                {

                    res.json({
                        code: 101,
                        message: "schematic recieved",
                        schematic: response.results
                    })

                }
                else
                {

                    res.status(404).json({
                        code: 404,
                        message: "schematic doesn't exists",
                    })

                }
            }
            catch(err)
            {

                console.log(chalk.bold(chalk.red(err)))

                res.status(404).json({
                    code: 404,
                    message: "schematic doesn't exists"
                })

            }

        })

    }
}

export default UserRoute;
import fs from 'fs'

class ErrorRoute {

    server = null;
    
    constructor(server) {

        this.server = server;
    }

    register() {

        const errorPage = fs.readFileSync("./views/404.html", () => {
            
        });

        this.server.app.use((req, res, next) => {
            
            if (
                !req.headers["user-agent"].includes("Chrome")
                && !req.headers["user-agent"].includes("Mozilla") 
                && !req.headers["user-agent"].includes("Edge")
                && !req.headers["user-agent"].includes("Safari")
            ) 
            {
                res.status(404).json({
                    message: "Path doesn't exist"
                })

                console.log('error');
            }
            else {

                res.setHeader('content-type', 'text/html');
                res.status(404).send(errorPage)
            }



        })

    }
}

export default ErrorRoute;
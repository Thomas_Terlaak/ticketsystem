import express from 'express'
import bodyparser from 'body-parser'
import fs from 'fs'
import bcrypt from 'bcrypt'

var path = require('path');
const saltRounds = 10;

class HomeRoute {

    server = null;
    
    constructor(server) {

        this.server = server;
    }

    register() {

        this.server.app.set('view engine', 'ejs')
        this.server.app.use(bodyparser.urlencoded({extended: true}))
        this.server.app.use(bodyparser.json());
        this.server.app.get('/', async (req, res) => {

            if (
                !req.headers["user-agent"].includes("Chrome") 
                && !req.headers["user-agent"].includes("Mozilla") 
                && !req.headers["user-agent"].includes("Edge") 
                && !req.headers["user-agent"].includes("Safari")
            ) 
            {
                res.json({

                    code: "200",
                    message: "Homepage requested"
                })
            }
            else {

                var loginurl = "/";
                res.render('pages/home', {url:loginurl});
            }
            
        })

        this.server.app.post('/', async (req, res) => {

            var username = req.body.username;
            var password = req.body.password;

            try
                {
                    let response = await this.server.dbConnector.query("SELECT * FROM users WHERE username = ?", username);
                    if(response.results != "")
                    {
                        let response = await this.server.dbConnector.query("SELECT * FROM users WHERE username = ? AND password = ?", [username, password]);
                        console.log(response.results)
                        if(response.results != "")
                        {
                            res.json({
                                code: 200,
                            })
                        }
                        else
                        {
                            res.json({
                                code: 404,
                            })
                        }
                    }
                    else
                    {
                        res.json({
                            code: 444,
                        })
                    }
                }
                catch(err)
                {
                    console.log(err)
                    res.json({
                        code: 404,
                    })
                }
            
        })
    }
}

export default HomeRoute;
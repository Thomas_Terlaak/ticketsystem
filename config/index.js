import Shared from './shared'
import Development from './development'
import Production from './production'

let extraConfig = {};

switch(process.env.NODE_ENV) {

    case "development":
        extraConfig = Development;
        break;

    case "production":
        extraConfig = Production;
        break;
}

export default {
    ...Shared,
    ...extraConfig
}
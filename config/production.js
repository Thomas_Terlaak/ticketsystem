export default {

    appName: "TicketSystem",
    version: "0.0.1",

    database: {

        host: "127.0.0.1",
        port: 3306,
        user: 'root',
        password: '',
        database: 'ticketsystem'
    }
    
}
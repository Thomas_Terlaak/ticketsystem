//Importing Packages
import express from 'express'
import chalk from 'chalk'

//import configs and Connectors
import DatabaseConnector from './DatabaseConnector.js'
import config from './config'
var path = require('path');

//importing Routes
import ErrorRoute from './routes/Error'
import HomeRoute from './routes/Home'

class Server {

    dbConnector = null;
    app = null;

    constructor() {
        //Constructing DBConnector and express
        this.dbConnector = new DatabaseConnector();
        this.app = express();
    }
    start() {

        this.app.use(express.static('public'))
        this.app.use('/public', express.static('public'))
        //Constructing homeRoute
        const homeRoute = new HomeRoute(this);
        homeRoute.register();

        //Constructing errorRoute
        const errorRoute = new ErrorRoute(this);
        errorRoute.register();

        //app startup
        this.app.listen(3000, () => {
            var dev = config.developer;
            console.log(chalk.bold(`${config.appName} version ${config.version} started!`));
            console.log(`API created by ` + chalk.bold(chalk.blue(chalk.bold(dev))) + ` all rights reserved!`);
        })
    }
}

export default Server;
import config from './config'
import mysql from 'mysql'

class DatabaseConnector {

    connection = null;

    constructor() {

        this.connection = mysql.createConnection({
            //Add database info to system variables.
            //Add database Api user/
            ...config.database
        });
    }

    destroy() {

        this.connection.destroy();
    }

    async query(query, params) {

        if (params == null) {

            params = {};
        }

        let val = (await new Promise(((resolve, reject) => {

            this.connection.query(query, params, function (error, results, fields) {

                if (error != null) {

                    reject(error);
                }

                resolve({

                    results,
                    fields
                });
            });
        })));

        return val;
    }
}

export default DatabaseConnector;